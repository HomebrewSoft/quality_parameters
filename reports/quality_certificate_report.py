from odoo import _, api, models
from odoo.exceptions import ValidationError

class QualityReport(models.AbstractModel):
    _name = 'report.quality_parameters.report_quality_certificate_template'


    # @api.model
    # def _get_report_values(self, docids, data=None):
    #     report_obj = self.env['ir.actions.report']
    #     report = report_obj._get_report_from_name('quality_parameters.report_quality_certificate_template')
    #     checks = self.env["quality.check"].browse(docids)
    #     if not checks._validate_parameters():
    #         raise ValidationError(_("Unvalid parameters: please fill every parameter before printing the certificate"))
    #     docargs = {
    #         'doc_ids': docids,
    #         'doc_model': report.model,
    #         'docs': checks,
    #     }
    #     return docargs


    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('quality_parameters.report_quality_certificate_template')
        checks = self.env["quality.check"].browse(docids)

        # Get the lot_id and product_id from the first check
        lot_id = checks[0].lot_id.id
        product_id = checks[0].product_id.id

        # Find all checks with the same lot_id and product_id
        same_product_lot_checks = self.env["quality.check"].search([
            ('lot_id', '=', lot_id),
            ('product_id', '=', product_id)
        ])

        # Find checks with parameters filled
        filled_checks = [check for check in same_product_lot_checks if check._validate_parameters()]

        # If no checks have parameters filled, raise an error
        if not filled_checks:
            raise ValidationError(_("No checks with filled parameters found for the same product and lot"))

        # Take the first check with parameters filled
        filled_check = filled_checks[0]

        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': filled_check,
        }
        return docargs