{
    "name": "quality_parameters",  # TODO
    "version": "16.0.0.3.0",  # TODO
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",  # TODO
    "license": "LGPL-3",
    "depends": [
        "account",
        "mrp",
        "base",
        "quality_control",
    ],  # TODO Check
    "data": [  # TODO Check
        # security
        "security/ir.model.access.csv",
        # data
        # reports
        "reports/quality_certificate_report.xml",
        # views
        "views/account_move.xml",
        "views/quality_check.xml",
        "views/quality_params.xml",
        "views/quality_point.xml",
        "views/product_template.xml",
        "views/mrp_production.xml",
    ],
}
