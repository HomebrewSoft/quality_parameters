from odoo import api, fields, models
from dateutil.relativedelta import relativedelta


class QualityCheck(models.Model):
    _inherit = "quality.check"

    quality_params_ids = fields.Many2many(
        comodel_name="quality.params",
    )

    folio = fields.Char(
        compute="_compute_folio_from_company",
        store=True,
    )
    manufacturing_date = fields.Date()
    expiration_date = fields.Date(
        compute="_compute_expiration_date",
        inverse="_inverse_expiration_date",
        store=True,
    )

    @api.depends("company_id", "manufacturing_date")
    def _compute_folio_from_company(self):
        for check in self:
            if check.manufacturing_date and not check.folio:
                check.folio = str(
                    check.company_id.quality_certificate_concecutive
                ).zfill(3)
                check.company_id.quality_certificate_concecutive += 1

    @api.depends("manufacturing_date")
    def _compute_expiration_date(self):
        for check in self:
            check.expiration_date = (
                check.manufacturing_date + relativedelta(years=2)
                if check.manufacturing_date
                else check.manufacturing_date
            )

    def _inverse_expiration_date(self):
        for check in self:
            check.manufacturing_date = (
                check.expiration_date - relativedelta(years=2)
                if not check.manufacturing_date
                else check.manufacturing_date
            )

    def _validate_parameters(self):
        for check in self:
            if not check.quality_params_ids:
                return False
            for param in check.quality_params_ids:
                if not (
                    param.product and param.min_qty and param.max_qty and param.real_qty
                ):
                    return False
        return True

    @api.model
    def create(self, vals):
        product_id = self.env["product.product"].browse(vals["product_id"])
        point_id = self.env["quality.point"].browse(vals["point_id"])
        new_params = []
        if product_id.enable_quality_control == True:
            for param in point_id.quality_params_ids:
                new_param = self.env["quality.params"].create(
                    {
                        "product": param.product,
                        "min_qty": param.min_qty,
                        "max_qty": param.max_qty,
                    }
                )
                new_params.append(new_param.id)
            vals.update({"quality_params_ids": new_params})
        return super(QualityCheck, self).create(vals)
