from odoo import fields, models

class Company(models.Model):
    _inherit = "res.company"

    quality_certificate_concecutive = fields.Integer(default=1)
