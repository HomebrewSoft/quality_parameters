from odoo import fields, models


class QualityParams(models.Model):
    _name = 'quality.params'

    product = fields.Char(string="Parameter")
    min_qty = fields.Char(string="Minimum")
    max_qty = fields.Char(string="Maximum")
    real_qty = fields.Char(string="Real")