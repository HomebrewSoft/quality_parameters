from odoo import fields, models


class QualityPoint(models.Model):
    _inherit = "quality.point"

    quality_params_ids = fields.Many2many(
        comodel_name="quality.params",
    )
    product_quality_check = fields.Boolean(
        # related="product_tmpl_id.enable_quality_control",
    )
