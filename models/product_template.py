from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = "product.template"

    enable_quality_control = fields.Boolean(string="Requiere Control De Calidad")
