import base64

from odoo.exceptions import ValidationError

from odoo import fields, models


class AccountMove(models.Model):
    _inherit = "account.move"


    def generate_quality_reports(self):
        self.ensure_one()

        # Find all the quality checks related to the invoice
        check_obj = self.env["quality.check"]
        pickings = self.env["stock.picking"].search([("origin", "=", self.invoice_origin)])
        lots = pickings.mapped("move_ids_without_package.move_line_ids.lot_id.id")
        quality_checks = check_obj.search([("lot_id", "in", lots)])

        # Find the first lot with complete parameters
        lot_with_params = None
        for lot_id in set(lots):
            lot_checks = quality_checks.filtered(lambda c: c.lot_id.id == lot_id)
            filled_checks = [check for check in lot_checks if check._validate_parameters()]
            if filled_checks:
                lot_with_params = (lot_id, filled_checks[0])
                break

        if not lot_with_params:
            raise ValidationError("Any lot have parameters complete")

        # Render report with the first lot that has complete parameters
        report_ref = 'quality_parameters.report_quality_certificate'
        res_ids = [lot_with_params[1].id]
        pdf_content, _ = self.env.ref(report_ref)._render_qweb_pdf(report_ref, res_ids=res_ids)

        # Generate attachment
        attachment = self.env["ir.attachment"].create({
            "name": f"{self.name}/QCI.pdf",
            "type": "binary",
            "datas": base64.b64encode(pdf_content),
            "store_fname": "Certificates",
            "res_model": "account.move",
            "res_id": self.id,
            "mimetype": "application/x-pdf",
        })

        return attachment

    def action_invoice_sent(self):
        self.ensure_one()
        wizard = super(AccountMove, self).action_invoice_sent()
        template = self.env.ref(
            "account.email_template_edi_invoice", raise_if_not_found=False
        )
        certificate = self.env["ir.attachment"].search(
            [("name", "=", f"{self.name}/QC.pdf")], limit=1
        )
        quality_certificates = template["attachment_ids"]
        for wrong_certificate in quality_certificates:
            if (
                wrong_certificate.name != f"{self.name}/QCA.pdf"
                and "/QCA" in wrong_certificate.name
            ):
                template.write({"attachment_ids": [(2, wrong_certificate.id)]})
        if certificate:
            template.write({"attachment_ids": [(4, certificate.id)]})
        return wizard
