from odoo import models, fields
from odoo.exceptions import UserError


class MrpProduction(models.Model):
    _inherit = "mrp.production"

    product_quality_check = fields.Boolean(
        related="product_id.enable_quality_control",
    )

    def _get_quality_check_values(self, quality_point):
        values = super(MrpProduction, self)._get_quality_check_values(quality_point)
        new_params = []
        for param in quality_point.quality_params_ids:
            new_param = self.env["quality.params"].create(
                {
                    "product": param.product,
                    "min_qty": param.min_qty,
                    "max_qty": param.max_qty,
                }
            )
            new_params.append(new_param.id)
        values.update({"quality_params_ids": new_params})
        return values
